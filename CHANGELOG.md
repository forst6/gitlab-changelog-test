## 0.0.2 (2021-03-01)

### Added (1 change)

- [added changlog_config](forst6/gitlab-changelog-test@7d1d54189c09a6fbf948b04ca185faab0993742b)

### Features (1 change)

- [added a third line](forst6/gitlab-changelog-test@a77caec58456f75820c0b342bc8e33c7348daa7b)

### bugfix (1 change)

- [added line four.](forst6/gitlab-changelog-test@4b05d7f243afa898af0217e15b23bf60f30269c1)

## 0.0.1 (2021-03-01)

No changes.
